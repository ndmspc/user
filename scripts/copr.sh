#!/bin/bash

PROJECT_DIR="$(dirname $(dirname $(readlink -m $0)))"
BASE_ARGS="ndmspc/stable -r fedora-40-x86_64"
copr-cli build $BASE_ARGS --nowait $PROJECT_DIR/scripts/SRPM/bash-git-prompt-2.7.1-1.fc38.src.rpm
copr-cli build $BASE_ARGS --nowait $PROJECT_DIR/scripts/SRPM/ndmspc-ansible-0.0.7-1.fc38.src.rpm
copr-cli build $BASE_ARGS --nowait $PROJECT_DIR/scripts/SRPM/zyre-2.0.1-3.src.rpm
copr-cli build $BASE_ARGS --nowait $PROJECT_DIR/scripts/SRPM/zmq2ws-1.20231012.0-1.fc39.src.rpm
copr-cli build $BASE_ARGS $PROJECT_DIR/scripts/SRPM/ndm-0.2.5-1.el9.src.rpm
copr-cli build $BASE_ARGS --nowait $PROJECT_DIR/scripts/SRPM/salsa-0.7.0-1.fc39.src.rpm

