# Kubernetes(k8s) cluster at JF


## Access to JF k8s cluster
If you own JF k8s certificate, one can skip this section. To create certificate by issuing following commands (e.g. on jf01.science.upjs.sk) just substitute `<username>` by your useraname (e.g. mvala)
```
export MY_USER=mvala
export MY_NAMESPACE=ndmspc
mkdir -p $HOME/.certs
openssl genrsa -out $HOME/.certs/tls.key 2048
openssl req -new -key $HOME/.certs/tls.key -out $HOME/.certs/tls.csr -subj "/CN=$MY_USER/O=$MY_NAMESPACE"
```
Then send certificate request `$HOME/.certs/tls.csr` to martin.vala@upjs.sk and you will get mail with you certificate and store it to `$HOME/.certs/tls.crt`

## Setup JF k8s on your laptop (desktop)
Make sure that you are in UPJS network or using UPJS VPN
```
export MY_USER=mvala
export MY_NAMESPACE=ndmspc
kubectl config set-cluster jf --server=https://jf01:6443 --certificate-authority=${HOME}/.certs/ca.crt
kubectl config set-credentials ${MY_USER} --client-certificate=${HOME}/.certs/tls.crt  --client-key=${HOME}/.certs/tls.key
kubectl config set-context ${MY_USER}-context --cluster=jf --namespace=${MY_NAMESPACE} --user=${MY_USER}
kubectl config use-context ${MY_USER}-context
```

## Testing JF k8s
One can get all pods/services running byt doing following command
```
$ kubectl get all
No resources found in ndmspc namespace.
```
This output means that everything is ok and not pods are running. You might see salsa cluster there with following output
```
$ kubectl get all
NAME                        READY   STATUS              RESTARTS   AGE
pod/slsd-b6fb98759-x5pr5    0/1     ContainerCreating   0          2s
pod/slsr-6bf8768f7-bxgsg    0/1     ContainerCreating   0          2s
pod/slsw-645b59588f-2jpxt   0/1     ContainerCreating   0          2s

NAME                            TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)           AGE
service/sls-discovery-service   ClusterIP   None             <none>        40000/TCP         2s
service/sls-mon-service         NodePort    10.101.168.123   <none>        5001:31865/TCP    2s
service/sls-submitter-service   NodePort    10.101.204.222   <none>        41000:30887/TCP   2s

NAME                   READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/slsd   0/1     1            0           2s
deployment.apps/slsr   0/1     1            0           2s
deployment.apps/slsw   0/1     1            0           2s

NAME                              DESIRED   CURRENT   READY   AGE
replicaset.apps/slsd-b6fb98759    1         1         0       2s
replicaset.apps/slsr-6bf8768f7    1         1         0       2s
replicaset.apps/slsw-645b59588f   1         1         0       2s
```
