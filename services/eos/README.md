# EOS client and server


## EOS client/fusex container.
1. Run install script as Superuser (root)
```bash
sudo su -
```

```bash
bash <(curl -s https://gitlab.com/ndmspc/user/-/raw/main/services/eos/eos_install)
```
2. Profit. :smirk_cat:

## EOS server
Creating/starting eos server by running

```
podman run -d --net host --name eos_server registry.gitlab.com/ndmspc/user/eos:server
```
Later one can stop container
```
podman stop eos_server
```
of start again
```
podman stop eos_server
```
One can enter `eos_server` by running

```
podman exec -it eos_server /bin/bash
```

And run some `eos` commands

### node ls
```
[root@fedora /]# eos node ls
┌──────────┬────────────────────────────────┬────────────────┬──────────┬────────────┬────────────────┬─────┐
│type      │                        hostport│          geotag│    status│   activated│  heartbeatdelta│ nofs│
└──────────┴────────────────────────────────┴────────────────┴──────────┴────────────┴────────────────┴─────┘
 nodesview                       fedora:1095       local::geo     online           on                0     1 
```
### fs ls

```
[root@fedora /]# eos fs ls
┌────────────────────────┬────┬──────┬────────────────────────────────┬────────────────┬────────────────┬────────────┬──────────────┬────────────┬──────┬────────┬────────────────┐
│host                    │port│    id│                            path│      schedgroup│          geotag│        boot│  configstatus│       drain│ usage│  active│          health│
└────────────────────────┴────┴──────┴────────────────────────────────┴────────────────┴────────────────┴────────────┴──────────────┴────────────┴──────┴────────┴────────────────┘
 fedora                   1095      1                         /data/01        default.0       local::geo       booted             rw      nodrain   8.00   online              N/A 
```
