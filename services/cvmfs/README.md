# About
Deploy CernVM File System (CVMFS) container.

## Via script
1. Run install script as Superuser (root)
```bash
sudo su -
```

```bash
bash <(curl -s https://gitlab.com/ndmspc/user/-/raw/main/services/cvmfs/cvmfs_install)
```
2. Profit. :smirk_cat:

## Misc
### Optional arguments to cvmfs_install script
|Option|Description|
|----|----|
|`<default>`|Default Installation|
|clean|Cleans current setup|
|reset|Install everything from scratch (Cleans current setup)|

### Overwrite image location
Run following command before installation
```
export CVMFS_IMAGE="registry.gitlab.com/ndmspc/user/cvmfs"
```

## k8s and cvmfs

## KIND cluster setup (optional)
If one uses KIND cluster one have to create config file
```
[root@localhost ~]# cat kind-config.yaml 
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  # add a mount from /path/to/my/files on the host to /files on the node
  extraMounts:
  - hostPath: /mnt/cvmfs
    containerPath: /mnt/cvmfs
    propagation: Bidirectional
```

and then create cluster

```
mkdir -p /mnt/cvmfs
kind create cluster --config kind-config.yaml
```

## Run cvmfs on k8s as daemonset
One can run cvmfs directly on k8s nodes. One can run it as admin
```
kubectl apply -f https://gitlab.com/ndmspc/user/-/raw/main/services/cvmfs/cvmfs.yaml
```
