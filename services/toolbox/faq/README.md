# Frequently Asked Questions about toolbox

- [Container specific commands](#container-specific-commands)

### Container specific commands

In a multi-container environment, I want to run container specific commands when entering each container. How to add container name based conditions in `~/.bashrc` or `~/.bash_profile` to run such commands?

* Each Container has a ``/etc/profile.d`` that contains files which are executed at startup by the shell. Create a startup script after entering each toolbox:
```shell
sudo vi /etc/profile.d/my_startup_script.sh
```
And modify the contents:
```shell
#!/bin/bash
export VAR1="val"
export VAR1="val"
source abc.sh
echo "Welcome to toolbox..."
```
Add execute permissions: 
```shell
chmod +x /etc/profile.d/my_startup_script.sh
```
Next time you login to the container the above commands will be executed and variables set for that specific toolbox container.
