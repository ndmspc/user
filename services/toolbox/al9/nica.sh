#!/bin/bash
export ALIBUILD_WORK_DIR=$HOME/nica/sw/202309
NICA_MODULES="/cvmfs/nica.jinr.ru/sw/202309/MODULES/$(aliBuild architecture)"

[ -d $NICA_MODULES ] && export MODULEPATH="$NICA_MODULES:$MODULEPATH"
