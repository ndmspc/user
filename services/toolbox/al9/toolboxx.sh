#!/bin/bash
export container_name=$(cat /run/.containerenv | grep name= | cut -d '"' -f2)
if [ -f $HOME/.config/toolbox/custom-prompt ];then
  source $HOME/.config/toolbox/custom-prompt
else
  PS1=$(printf "\[\033[35m\]⬢\[\033[0m\]%s" "[\u@\h [$container_name] \W]\\$ ")
fi
