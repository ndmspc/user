# Toolbox images

| OS        | Image                                |
| --------- | ------------------------------------ |
| Fedora 41 | registry.gitlab.com/ndmspc/user/fc41 |
| Fedora 40 | registry.gitlab.com/ndmspc/user/fc40 |
| Alma 9    | registry.gitlab.com/ndmspc/user/al9  |

## Creating toolbox from image

```shell
toolbox create -c <container-name> -i <image-name> -y
```

## Entering toolbox

```shell
toolbox enter <container-name>
```

Check your working environment with

```shell
cat /etc/system-release
```

## Fedora XX toolbox (XX is 41, 40)

Substitute XX by Fedora version you are running on OS.
To create

```shell
toolbox create -c fedora-toolbox-41 -i registry.gitlab.com/ndmspc/user/fc41 -y
```

To enter

```shell
toolbox enter
```

## Fedora 40 toolbox (as second container)

To create

```shell
toolbox create -c fedora-toolbox-40 -i registry.gitlab.com/ndmspc/user/fc40 -y
```

To enter

```shell
toolbox enter fedora-toolbox-40
```

## AlmaLinux 9 toolbox

To create

```shell
toolbox create -c al9 -i registry.gitlab.com/ndmspc/user/al9:alibuild-dev -y
```

To enter

```shell
toolbox enter al9
```
