# Post process

- Create new fedora target (eg. fedora-40-x86_64) in copr [`ndmspc/stable`](https://copr.fedorainfracloud.org/coprs/ndmspc/stable) repo in [setting](https://copr.fedorainfracloud.org/coprs/ndmspc/stable/edit/)
- Run `scripts/corp.sh` (look and modify newer versions)
- List of packages needs to be rebuilt:
  - `bash-git-prompt`
  - `ndmspc-ansible`
  - `zyre`
  - `ndm`
  - `ndh`
  - `salsa` (needs `ndm`)

- List of optionl packages:  
  - `zmq2ws`