#!/bin/bash
container_name=$(cat /run/.containerenv | grep name= | cut -d '"' -f2)
export container_name=${container_name/fedora-toolbox-/fc}

if [ -f $HOME/.config/toolbox/custom-prompt ];then
  source $HOME/.config/toolbox/custom-prompt
else
  [ -n "$container_name" ] && PS1=$(printf "\[\033[35m\]⬢\[\033[0m\]%s" "[\u@\h [$container_name] \W]\\$ ")
fi
