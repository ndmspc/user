# Základy programovania v "bash"

**Bash** je príkazový interpreter určený na spravovanie súborov (vytváranie súborov a ich premiestňovanie, mazanie, kopírovanie, ...); má limitovanú syntax, nevýhodný pri zložitejších výpočtoch. 

**Terminál** (command prompt) slúži ako komunikačný nástroj medzi užívateľom a operačným systémom. Komunikácia prebieha pomocou **príkazov**.


### Príklady najčastejšie používaných príkazov

##### man [command]
- manuál
- vypíše a charakterizuje všetky krátke (napr. `-a`) a dlhé argumenty (napr. `--all`) k danému príkazu
- pr. manuál ku príkazu ls:
  ```
  man ls
  ```
  ![](images/man-ls.png)

##### ls
- vypíše obsah adresára, v ktorom sa aktuálne nachádzame
- pr. vypíš celý obsah adresára v "long-listing" formáte
  ```
  ls -al
  ```
  čo je to isté ako
  ```
  ls --all -l
  ```
  alebo 
  ```
  ls -a -l
  ```
  ```
  ⬢[ktropp@toolbox jf]$ ls -al
  total 12
  drwxr-xr-x. 1 ktropp ktropp  54 Oct  8 09:44 .
  drwx------. 1 ktropp ktropp 518 Oct 14 14:33 ..
  -rw-r--r--. 1 ktropp ktropp  38 Oct  5 15:40 README.md
  -rwxr-xr-x. 1 ktropp ktropp 782 Oct 14 14:13 test2.sh
  -rwxr-xr-x. 1 ktropp ktropp 118 Oct  5 15:57 test.sh
  drwxr-xr-x. 1 ktropp ktropp  28 Oct 14 13:39 tmp
  ```

##### pwd
- zobrazí plnú (**absolútnu**) cestu k adresáru, v ktorom sa aktuálne nachádzame - začína sa lomítkom 
  ```
  ⬢[ktropp@toolbox jf]$ pwd
  /var/home/ktropp/jf
  ```

##### tree
- zobrazí štruktúru adresára, v ktorom sa aktuálne nachádzame, vo forme stromového diagramu
  ```
  ⬢[ktropp@toolbox jf]$ tree
  .
  ├── README.md
  ├── test2.sh
  ├── test.sh
  └── tmp
      └── aaa

  ```

##### cd [directory]
- slúži na prechod medzi adresármi
- k adresáru, do ktorého chceme prejsť, môžeme zadať **absolútnu** cestu
  ```
  ⬢[ktropp@toolbox jf]$ cd /var/home/ktropp/jf/tmp
  ```
  alebo **relatívnu** cestu vzhľadom na adresár, v ktorom sa aktuálne nachádzame
  ```
  ⬢[ktropp@toolbox jf]$ cd tmp
  ```
- ak sa chceme dostať do domáceho adresára (/var/home/ktropp), môžeme namiesto absolútnej cesty použiť `cd ~`
- ak sa chceme dostať o adresár vyššie (späť), môžeme použiť `cd ..` 
  
  > Symbol `.` predstavuje adresár (resp. cestu k nemu), v  ktorom sa aktuálne nachádzame. Symbol `..` znamená "o adresár vyššie".

##### mkdir [directory]
- vytvorí nový adresár
- pr. vytvor adresár s názvom jf v adresári, v ktorom sa aktuálne nachádzam
  ```
  mkdir jf
  ```
- pre vytváranie viacúrovňových adresárov môžeme použiť argument `-p`:
  ```
  mkdir -p 01/001 02/001 02/002
  ```

##### rmdir [directory]
- vymaže adresár
- pr.:
  ```
  ⬢[ktropp@toolbox tmp]$ tree
  .
  ├── 01
  │   └── 001
  ├── 02
  │   ├── 001
  │   └── 002
  └── aaa
  ``` 
  - vymaž adresár `001` v adresári `01`, ktorý obsahuje iba súbory
    ```
    rmdir 01/001
    ```
  - vymaž adresár `02`, ktorý obsahuje aj ďalšie adresáre - rekurzívne mazanie
    ```
    rmdir -r 02
    ```

##### rm [file/directory]
- vymaže súbor alebo adresár

##### touch [filename]
- vytvorí prázdny súbor

##### cat [filename]
- zobrazí obsah súboru v termináli (za posledným príkazom)

##### less [filename]
- zobrazí obsah súboru v novom okne
- zobrazovanie ukončíme stlačením <kbd>Q</kbd>

##### head [filename]
- zobrazí prvých 10 riadkov z obsahu súboru
  ```
  ⬢[ktropp@toolbox jf]$ head test2.sh 
  #!/bin/bash

  # ak hodnota premennej FILENAME nie je zadana zvonku, defaultna hodnota bude "/tmp/aaa" 
  FILENAME=${FILENAME-"/tmp/aaa"}

  # vypiseme hodnotu systemovej premennej USER
  echo $USER

  # vypiseme hodnotu systemovej premennej HOME
  echo $HOME

  ```
- 10 je "default" hodnota, ak chceme zmeniť počet zobrazovaných riadkov, napr. na 5, použijeme:
  ```
  head -n 5 test2.sh
  ```

##### tail [filename]
- zobrazí posledných 10 riadkov z obsahu súboru
  ```
  ⬢[ktropp@toolbox jf]$ tail test2.sh 
  for i in $*; do
    # zapiseme "/home/ktropp/$i.root" do suboru s nazvom $FILENAME
    echo "/home/ktropp/$i.root" >> $FILENAME
  done

  # vypiseme nazov suboru 
  echo $FILENAME

  # vypiseme obsah daneho suboru 
  cat $FILENAME

  ```
- 10 je "default" hodnota, ak chceme zmeniť počet zobrazovaných riadkov, napr. na 5, použijeme:
  ```
  tail -n 5 test2.sh
  ```
    
##### export
- zobrazí systémové premenné - ich názvy sa píšu veľkými písmenami

##### echo [string]
- vypíše zadaný text
- hodnota premennej -> pred meno premennej dáme $
- pr. vypíš hodnotu premennej USER
  ```
  ⬢[ktropp@toolbox jf]$ echo Hodnota premennej USER je $USER
  Hodnota premennej USER je ktropp
  ```

### Príklady skriptov v jazyku bash
V prvom riadku musí byť:
```
#! /bin/bash
```
> Symbolom `#` sa označuje komentár, ale symbol `#!` upozorňuje akým jazykom má byť spustený skript. 

Pred spustením skriptu si overíme jeho vlastnosti (práva) - ak po zadaní príkazu `ls -l` v 1. stĺpci výstupu (a riadku, ktorý patrí k danému skriptu) **chýba `x`**, skript nie je určený na spúšťanie, **nie je "executable"**. V takomto prípade môžeme pomocou príkazu `chmod +x test.sh` dodať užívateľom právo na spustenie.

Skript spúšťame plnou cestou pomocou:

  a) `/var/home/ktropp/jf/test.sh`

  b) pokiaľ sme v adresári `jf` stačí použiť
     `./test.sh`


#### Pr. 1: `test.sh` - vypísanie zadaných informácií v termináli 
  ```
  ⬢[ktropp@toolbox jf]$ cat test.sh
  #!/bin/bash

  # Print PC running time
  uptime

  # Prints hostname
  hostname

  # Sleeps 2 sec
  sleep 2

  # disk free space
  df
  ```

  ##### Ukážka výstupu pre spustenie skriptu `test.sh`:
  ```
  ⬢[ktropp@toolbox jf]$ ./test.sh 
  14:01:28 up  4:39,  0 users,  load average: 0.38, 0.70, 0.73
  toolbox
  Filesystem     1K-blocks     Used Available Use% Mounted on
  fuse-overlayfs 248394752 54588000 192835520  23% /
  devtmpfs         3969872        0   3969872   0% /dev
  tmpfs            4004812   545508   3459304  14% /dev/shm
  tmpfs            1601928    10152   1591776   1% /run/media
  tmpfs             800960      208    800752   1% /run/secrets
  /dev/sda3      248394752 54588000 192835520  23% /var/mnt
  tmpfs            4004816     3480   4001336   1% /tmp
  /dev/sda2         999320    76080    854428   9% /run/host/boot
  /dev/sda1         613160     8728    604432   2% /run/host/boot/efi
  /dev/sda3      248394752 54588000 192835520  23% /var/home/ktropp
  cvmfs2           5120000  1493815   3626185  30% /run/host/var/cvmfs/sft.cern.ch
  cvmfs2           5120000  1493815   3626185  30% /run/host/var/cvmfs/alice.cern.ch
  cvmfs2           5120000  1493815   3626185  30% /run/host/var/cvmfs/alice-nightlies.cern.ch
  cvmfs2           5120000  1493815   3626185  30% /run/host/var/cvmfs/atlas.cern.ch
  cvmfs2           5120000  1493815   3626185  30% /run/host/var/cvmfs/geant4.cern.ch
  ```

#### Pr. 2: `test2.sh` - práca so systémovými premennými, for cyklus
  ```
  ⬢[ktropp@toolbox jf]$ cat test2.sh
  #!/bin/bash
  
  # ak hodnota premennej FILENAME nie je zadana zvonku, defaultna hodnota bude "/tmp/aaa" 
  FILENAME=${FILENAME-"/tmp/aaa"}

  # vypiseme hodnotu systemovej premennej USER
  echo $USER

  # vypiseme hodnotu systemovej premennej HOME
  echo $HOME

  # vypiseme pocet zadanych argumentov pomocou symbolu #
  echo "Number of arguments: $#"

  # preloopujeme cez vsetky zadane argumenty pomocou *
  echo "Argumets: $*"

  # ak existuje subor s menom $FILENAME, tak ho vymazeme
  rm -f $FILENAME
  
  # for cyklus - loopujeme cez argumenty
  for i in $*; do
    # zapiseme "/home/ktropp/$i.root" do suboru s nazvom $FILENAME
    echo "/home/ktropp/$i.root" >> $FILENAME
  done

  # vypiseme nazov suboru 
  echo $FILENAME

  # vypiseme obsah daneho suboru 
  cat $FILENAME
  ```

  ##### Ukážka výstupu pre spustenie skriptu `test2.sh`:
  ```
  ⬢[ktropp@toolbox jf]$ ./test2.sh 1 2 3 3333
  ktropp
  /var/home/ktropp
  Number of arguments: 4
  Argumets: 1 2 3 3333
  /tmp/aaa
  /home/ktropp/1.root
  /home/ktropp/2.root
  /home/ktropp/3.root
  /home/ktropp/3333.root
  ```
